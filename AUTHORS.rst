=======
Credits
=======

Development Lead
----------------

* Nicolas Cordier <nicolas.cordier@numeric-gmbh.ch>

Contributors
------------

None yet. Why not be the first?
